set :stage, :production

# Simple Role Syntax
# ==================
# Supports bulk-adding hosts to roles, the primary
# server in each group is considered to be the first
# unless any hosts have the primary property set.

# Extended Server Syntax
# ======================
# This can be used to drop a more detailed server
# definition into the server list. The second argument
# something that quacks like a hash can be used to set
# extended properties on the server.


ask :myport, proc { "22".chomp }
ask :mypass, proc { "******".chomp }
ask :host, proc { "exemplo: 123.123.123.123".chomp }

server fetch(:host, '127.0.0.1'), user: 'horus', roles: %w{web app db}

set :deploy_to, '/home/horus'
set :use_sudo, false
set :pty, false
set :rails_env, :production

# you can set custom ssh options
# it's possible to pass any option but you need to keep in mind that net/ssh understand limited list of options
# you can see them in [net/ssh documentation](http://net-ssh.github.io/net-ssh/classes/Net/SSH.html#method-c-start)
# set it globally
set :ssh_options, {
    user: 'horus',
    forward_agent: false,
    port: fetch(:myport, '22'),
    password: 'horusfilespass'
}
# and/or per server
# server 'example.com',
#   user: 'user_name',
#   roles: %w{web app},
#   ssh_options: {
#     user: 'user_name', # overrides user setting above
#     keys: %w(/home/user_name/.ssh/id_rsa),
#     forward_agent: false,
#     auth_methods: %w(publickey password)
#     # password: 'please use keys'
#   }
# setting per server overrides global ssh_options

# fetch(:default_env).merge!(rails_env: :production)

# RBENV Definitions
set :rbenv_type, :user # or :system, depends on your rbenv setup
set :rbenv_ruby, '2.1.5'
set :rbenv_prefix, "RBENV_ROOT=#{fetch(:rbenv_path)} RBENV_VERSION=#{fetch(:rbenv_ruby)} #{fetch(:rbenv_path)}/bin/rbenv exec"
set :rbenv_map_bins, %w{rake gem bundle ruby rails}
set :rbenv_roles, :all # default value
set :rbenv_setup, true
set :rbenv_force_pull, true

# Puma Definitions
set :puma_threads, [3, 6]
set :puma_workers, 3
# set :puma_init_active_record, true
set :puma_bind, "tcp://127.0.0.1:7001"