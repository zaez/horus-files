Horusfile::Application.routes.draw do

  post '/archives/upload', to: 'archives#upload'
  post '/archives/preview', to: 'archives#upload_preview'

  defaults format: :json do
    post '/archives/find' => 'archives#index'
    resources :archives
    resources :categories
    resources :tags, only: [:index]
  end

  get '/:id/download',          to: 'archives#download',               format: :zip
  get '/:id/preview.png',       to: 'archives#dispatch_preview',       format: :png
  get '/:id/thumb_preview.png', to: 'archives#dispatch_thumb_preview', format: :png

  match '/*path', to: 'application#options', via: :options

end
