class Tag
  include Mongoid::Document

  field :name, type: String

  def archives_count
    Archive.where({:tag_ids.in => [id.to_s]}).count
  end

  def to_indexed_json
    {
        name: I18n.transliterate(name)
    }.to_json
  end
end