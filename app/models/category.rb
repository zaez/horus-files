class Category
  include Mongoid::Document
  field :name, type: String
  has_and_belongs_to_many :archives

  after_save :elasticsearch_reindex
  after_update :elasticsearch_reindex

  def to_indexed_json
    {
        name: I18n.transliterate(name)
    }.to_json
  end

  def elasticsearch_reindex
    Archive.where({:category_ids.in => [id]}).each do |archive|
      archive.elasticsearch_reindex
    end
  end

end

