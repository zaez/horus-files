require 'elasticsearch/model'
require 'zip'

class Archive
  include Mongoid::Document
  include Mongoid::Timestamps
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks

  field :name, type: String
  field :download, type: Integer, default: 1
  field :size, type: Integer

  mount_uploader :preview, PreviewUploader

  has_and_belongs_to_many :tags
  has_and_belongs_to_many :categories

  before_destroy :delete_from_storage

  def url(real=false)
    return "#{Horusfile::CONFIG['host']}/#{id}/download" unless real
    "#{Horusfile::CONFIG['host']}/uploads/files/#{id}.zip"
  end

  def has_file?
    File.exist? filename
  end

  def has_preview?
    File.exist? File.join(uploads_dir, 'preview', "#{id}.jpg")
  end

  def storage(name, file)
    delete_from_storage

    # Prepare folder when not exist
    FileUtils.mkdir_p File.dirname(filename)

    # Catch path name of file
    filepath = file.is_a?(ActionDispatch::Http::UploadedFile) ? file.path : file.to_s

    unless name.end_with?('.zip')
      Zip::File.open(filename, Zip::File::CREATE) do |zipfile|
        # Two arguments:
        # - The name of the file as it will appear in the archive
        # - The original file, including the path to find it
        zipfile.add(name, filepath )
      end
    else
      FileUtils.move filepath, filename
    end
    self.size = File.size filename
    self.name = name if self.name.nil?
  end

  def as_indexed_json(options={})
    {
        name: I18n.transliterate(name || ''),
        created_at: created_at,
        download: download || 1,
        tags: tags.map {|t| {name: t.name, id: t.id.to_s } },
        categories: categories.map {|c| { name: c.name, id: c.id.to_s} }
    }
  end

  def self.s(organic=nil, _categories=nil, _tags=nil, options={})
    options[:size] = options.delete(:per_page) || 4
    options[:from] = options[:size] * ((options.delete(:page) || 1)-1)
    options[:query] = {
        function_score: {
            # functions: [
            #     {
            #         script_score: {
            #             lang: 'groovy',
            #             script: "_score * (doc['download'].value * 0.004 + 1)"
            #         }
            #     }
            # ]
        }
    }
    if not organic.nil? and not organic.empty?
      options[:query][:function_score][:query] = {
          multi_match: {
              query: "#{organic}",
              fields: ['name^2', '_all']
          }
      }
    end
    unless _categories.nil?
      options[:query][:function_score][:filter] ||= {and: {filters: []}}
      _categories.each { |id| options[:query][:function_score][:filter][:and][:filters] << {term: {'categories.id'=> id} } }
    end
    unless _tags.nil?
      options[:query][:function_score][:filter] ||= {and: {filters: []}}
      _tags.each { |id| options[:query][:function_score][:filter][:and][:filters] << {term: {'tags.id'=> id} } }
    end
    a = search options
    a.records.to_a
  end

  private

  def uploads_dir
    File.join Horusfile::CONFIG['path'], 'uploads'
  end

  def filename
    File.join uploads_dir, 'files', "#{id}.zip"
  end

  def delete_from_storage
    # Removes file if exists
    [
        filename,
        File.join(uploads_dir, 'preview', "#{id}.jpg"),
        File.join(uploads_dir, 'preview', "thumb_#{id}.jpg")
    ].each do |file|
      File.delete(file) if File.exist? file
    end
  end
end
