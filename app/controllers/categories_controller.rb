class CategoriesController < ApplicationController
  before_action :set_category, only: [:show, :destroy, :edit, :update]

  def index
    @categories = Category.all.order ['name ASC']
  end

  def show
  end

  def edit
  end

  def create
    @category = Category.new category_params
    if @category.save
      render action: 'show.json.rabl'
    else
      render status: 500, json: {status: :ERROR, message: @category.errors.full_messages }
    end
  end

  def destroy
    @category.destroy
    render json: {message: 'removido'}
  end

  def update
    if @category.update(category_params)
      render action: 'show.json.rabl'
    else
      render status: 500, json: {status: :ERROR, message: @category.errors.full_messages}
    end
  end

  protected
  def set_category
    @category = Category.find(params[:id])
  end

  def category_params
    params.require(:category).permit(:name)
  end

end
