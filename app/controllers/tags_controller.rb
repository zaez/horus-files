class TagsController < ApplicationController

  def index
    @tags = Tag.all.order ['name ASC']
  end

end
