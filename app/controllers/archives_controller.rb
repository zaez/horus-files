class ArchivesController < ApplicationController
  before_action :set_archive, only: [:show, :destroy, :edit, :update, :download]

  # GET /archive
  def index
    Archive.paginates_per params[:itens_per_page].present? ? params[:itens_per_page].to_i : 4
    options = {
        :per_page => params[:itens_per_page] ? params[:itens_per_page].to_i : 4,
        :page => (params[:page] || 1).to_i
    }
    @archives = Archive.s(params[:search], params[:category_ids], params[:tag_ids], options)
  end

  # GET /archive/1/edit
  def edit
  end

  def download
    @archive.download = @archive.download + 1
    @archive.save
    redirect_to @archive.url(true)
  end

  # POST /archive
  def create
    @archive = Archive.new archive_params
    if @archive.save
      build_tags
      build_categories
      render action: 'show.json.rabl'
    else
      render status: 500, json: {status: :ERROR, message: @archive.errors.full_messages }
    end
  end

  # DELETE /archive/1
  def destroy
    authorize @archive
    @archive.to_inactive!
    render json: {message: 'removido'}
  end

  # PATCH /archive/1
  def update
    if @archive.update(archive_params)
      build_tags
      build_categories
      render json: {status: :OK, archive: @archive.attributes}
    else
      render status: 500, json: {status: :ERROR, message: @archive.errors.full_messages}
    end
  end

  # POST /archives/upload
  def upload
    logger = Logger.new Rails.root.join('log','upload.log')
    unless params[:archive_id].present?
      @archive = Archive.new
    else
      @archive = Archive.find params[:archive_id]
    end
    ext = File.extname(params[:name]).downcase
    @archive.storage params[:name], params[:file]
    @archive.preview.store!(params[:file]) if %w[.jpg .jpeg .png .gif .bmp].include?(ext)
    if @archive.save
      if params[:id].present?
        render action: 'show'
      else
        render action: 'show.json.rabl'
        # render json: {status: :OK, archive: {id: @archive.id.to_s}}
      end
    else
      render status: 500, json: {status: :ERROR, message: @archive.errors.full_messages}
    end
  end

  # POST /archives/upload_preview
  def upload_preview
    unless params[:archive_id].present?
      @archive = Archive.new
    else
      @archive = Archive.find params[:archive_id]
    end
    @archive.preview.store! params[:file]
    if @archive.save
      if params[:id].present?
        render action: 'show'
      else
        render action: 'show.json.rabl'
      end
    else
      render status: 500, json: {status: :ERROR, message: @archive.errors.full_messages}
    end
  end

  protected

  def build_tags
    if @archive and params[:archive][:tags].present?
      @archive.tag_ids = params[:archive][:tags].map {|tag| Tag.find_or_create_by(name: tag['text'].downcase).id }
      @archive.save
    end
  end

  def build_categories
    if @archive and params[:archive][:category_ids].present?
      @archive.category_ids = params[:archive][:category_ids]
      @archive.save
    end
  end

  def set_archive
    @archive = Archive.find(params[:id] || params[:archive_id])
  end

  def archive_params
    params.require(:archive).permit(:name, :image, :status)
  end

end
