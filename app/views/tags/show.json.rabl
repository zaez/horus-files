object @tag

attributes :name

node :id do |t|
  t._id.to_s
end

node :count do |t|
  t.archives_count
end