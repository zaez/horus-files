object @archive

attributes :name, :size

node :id do |id|
  id._id.to_s
end

node :category_ids do |arch|
  arch.categories.map{|c| c.id.to_s }
end

node :categories do |arch|
  arch.categories.map{|c| c.name }
end

node :tags do |arch|
  arch.tags.map{|c| {text: c.name} }
end


node :url do |id|
  id.url.to_s if id.has_file?
end


node :preview do |arch|
  arch.has_preview? ? arch.preview.url : 'http://dummyimage.com/700x550/e4e3e6/9e9e9e.jpg&text=no+image'
end

node :thumb_preview do |arch|
  arch.has_preview? ? arch.preview.thumb.url : 'http://dummyimage.com/300x200/e4e3e6/9e9e9e.jpg&text=no+image'
end
