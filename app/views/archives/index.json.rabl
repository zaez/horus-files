node(:total) { Archive.count }
node(:total_entries) { @archives.count }
node(:itens_per_page) { Archive.default_per_page }

node :data do
  partial('archives/show', object: @archives)
end