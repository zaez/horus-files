object @category

attributes :name

node :id do |cat|
  cat._id.to_s
end

node :count do |cat|
  cat.archives.count
end