namespace :assets do
	desc 'Copia todos os assets para a versao sem digest'
	task non_digested: :environment do
	  assets = Dir.glob(File.join(Rails.root, 'public/assets/**/*'))
	  regex = /(-{1}[a-z0-9]{32}*\.{1}){1}/
	  assets.each do |file|
	    next if File.directory?(file) || file !~ regex

	    source = file.split('/')
	    source.push(source.pop.gsub(regex, '.'))

	    non_digested = File.join(source)

	    next if non_digested.include? 'manifest.json'
	    FileUtils.rm non_digested if File.exist? non_digested
	    FileUtils.cp file, non_digested
	  end
	end
end